from bakery.rest_api_demo.database import db


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    value = db.Column(db.Float)
    category = db.Column(db.String(80))

    def __init__(self, name, category, value):
        self.name = name
        self.value = value
        self.category = category

    def __repr__(self):
        return '<Product %r>' % self.name
