from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def reset_database():
    from bakery.rest_api_demo.database.product import Product
    from bakery.rest_api_demo.database.sale import Sale

    db.drop_all()
    db.create_all()
