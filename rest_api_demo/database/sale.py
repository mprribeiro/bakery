from datetime import datetime
from bakery.rest_api_demo.database import db


class Sale(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sale_date = db.Column(db.DateTime)
    code = db.Column(db.String(50))

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    product = db.relationship('Product', backref=db.backref('sale', lazy='dynamic'))

    def __init__(self, code, product, sale_date=None):
        self.code = code
        if sale_date is None:
            sale_date = datetime.utcnow()
        self.sale_date = sale_date
        self.product = product

    def __repr__(self):
        return '<Sale %r>' % self.code
