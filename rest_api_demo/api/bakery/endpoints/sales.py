import logging

from flask import request
from flask_restplus import Resource
from bakery.rest_api_demo.api.bakery.business import register_sale, delete_sale
from bakery.rest_api_demo.api.bakery.parsers import pagination_arguments
from bakery.rest_api_demo.api.bakery.serializers import sale, sale_with_products
from bakery.rest_api_demo.api.restplus import api
from bakery.rest_api_demo.database.sale import Sale

log = logging.getLogger(__name__)

ns = api.namespace('bakery/sales', description='Operations related to bakery sales')


@ns.route('/')
class SaleCollection(Resource):

    @api.marshal_list_with(sale)
    def get(self):
        """
        Returns list of bakery sales.
        """
        sales = Sale.query.all()
        return sales

    @api.response(201, 'Sale successfully registered.')
    @api.expect(sale)
    def sale(self):
        """
        Register a ner sale.
        """
        data = request.json
        register_sale(data)
        return None, 201


@ns.route('/<int:id>')
@api.response(404, 'Sale not found.')
class SaleItem(Resource):

    @api.marshal_with(sale_with_products)
    def get(self, id):
        """
        Returns a sale with a list of products.
        """
        return Sale.query.filter(Sale.id == id).one()

    @api.response(204, 'Sale successfully deleted.')
    def delete(self, id):
        """
        Deletes bakery sale.
        """
        delete_sale(id)
        return None, 204


@ns.route('/archive/<int:year>/')
@ns.route('/archive/<int:year>/<int:month>/')
@ns.route('/archive/<int:year>/<int:month>/<int:day>/')
class SalesArchiveCollection(Resource):

    @api.expect(pagination_arguments, validate=True)
    @api.marshal_with(sale_with_products)
    def get(self, year, month=None, day=None):
        """
        Returns list of sales from a specified time period.
        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)

        start_month = month if month else 1
        end_month = month if month else 12
        start_day = day if day else 1
        end_day = day + 1 if day else 31
        start_date = '{0:04d}-{1:02d}-{2:02d}'.format(year, start_month, start_day)
        end_date = '{0:04d}-{1:02d}-{2:02d}'.format(year, end_month, end_day)
        sale_query = Sale.query.filter(Sale.sale_date >= start_date).filter(Sale.sale_date <= end_date)

        sale_page = sale_query.paginate(page, per_page, error_out=False)

        return sale_page
