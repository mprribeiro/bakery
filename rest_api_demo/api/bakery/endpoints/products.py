import logging

from flask import request
from flask_restplus import Resource
from bakery.rest_api_demo.api.bakery.business import register_product, delete_product
from bakery.rest_api_demo.api.bakery.serializers import bakery_product, page_of_bakery_products
from bakery.rest_api_demo.api.bakery.parsers import pagination_arguments
from bakery.rest_api_demo.api.restplus import api
from bakery.rest_api_demo.database.product import Product

log = logging.getLogger(__name__)

ns = api.namespace('bakery/products', description='Operations related to bakery products')


@ns.route('/')
class ProductCollection(Resource):

    @api.expect(pagination_arguments)
    @api.marshal_with(page_of_bakery_products)
    def get(self):
        """
        Returns list of bakery products.
        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)

        products_query = Product.query
        products_page = products_query.paginate(page, per_page, error_out=False)

        return products_page

    @api.expect(bakery_product)
    def set(self):
        """
        Register a new product.
        """
        register_product(request.json)
        return None, 201


@ns.route('/<int:id>')
@api.response(404, 'Product not found.')
class ProductItem(Resource):

    @api.marshal_with(bakery_product)
    def get(self, id):
        """
        Returns a bakery product.
        """
        return Product.query.filter(Product.id == id).one()

    @api.response(204, 'Product successfully deleted.')
    def delete(self, id):
        """
        Delete product.
        """
        delete_product(id)
        return None, 204
