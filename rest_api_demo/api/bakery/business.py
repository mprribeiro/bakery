from bakery.rest_api_demo.database import db
from bakery.rest_api_demo.database.product import Product
from bakery.rest_api_demo.database.sale import Sale


def register_product(data):
    name = data.get('name')
    value = data.get('value')
    category = data.get('category')
    product = Product(name, category, value)
    db.session.add(product)
    db.session.commit()


def delete_product(product_id):
    product = Product.query.filter(Product.id == product_id).one()
    db.session.delete(product)
    db.session.commit()


def register_sale(data):
    code = data.get('products_list')
    sale_id = data.get('id')
    product_id = data.get('product_id')
    product = Product.query.filter(Product.id == product_id).one()

    sale = Sale(code, product)
    if sale_id:
        sale.id = sale_id

    db.session.add(sale)
    db.session.commit()


def delete_sale(sale_id):
    sale = Sale.query.filter(Sale.id == sale_id).one()
    db.session.delete(sale)
    db.session.commit()
