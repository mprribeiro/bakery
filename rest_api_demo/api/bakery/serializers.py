from flask_restplus import fields
from bakery.rest_api_demo.api.restplus import api

bakery_product = api.model('Bakery product', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of a bakery product'),
    'name': fields.String(required=True, description='Product name'),
    'value': fields.Float(required=True, description='Product value'),
    'category': fields.String(required=True, description='Product category'),
})

pagination = api.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

page_of_bakery_products = api.inherit('Page of bakery products', pagination, {
    'items': fields.List(fields.Nested(bakery_product))
})

sale = api.model('Bakery sale', {
    'id': fields.Integer(readOnly=True, description='The unique identifier of a blog category'),
    'code': fields.String(required=True, description='Sale code'),
    'product_id': fields.Integer(attribute='product.id'),
    'product': fields.String(attribute='product.name'),
    'sale_date': fields.DateTime,
})

sale_with_products = api.inherit('Bakery sale with products', sale, {
    'products': fields.List(fields.Nested(bakery_product))
})
