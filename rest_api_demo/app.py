import logging.config
import os

from flask import Flask, Blueprint
from bakery.rest_api_demo import configuration
from bakery.rest_api_demo.api.bakery.endpoints.products import ns as bakery_products_namespace
from bakery.rest_api_demo.api.bakery.endpoints.sales import ns as bakery_sales_namespace
from bakery.rest_api_demo.api.restplus import api
from bakery.rest_api_demo.database import db

app = Flask(__name__)
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '../logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)


def configure_app(flask_app):
    flask_app.config['SERVER_NAME'] = configuration.FLASK_SERVER_NAME
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = configuration.SQLALCHEMY_DATABASE_URI
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = configuration.SQLALCHEMY_TRACK_MODIFICATIONS
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = configuration.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = configuration.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = configuration.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = configuration.RESTPLUS_ERROR_404_HELP


def initialize_app(flask_app):
    configure_app(flask_app)

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(bakery_products_namespace)
    api.add_namespace(bakery_sales_namespace)
    flask_app.register_blueprint(blueprint)


def main():
    initialize_app(app)
    db.init_app(app)
    log.info('>>>>> Starting development server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
    app.run(debug=configuration.FLASK_DEBUG)


if __name__ == "__main__":
    main()
